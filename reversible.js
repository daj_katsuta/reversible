"use strict";

var WeightData = [
    [ 30, -12,  0, -1, -1,  0, -12,  30],
    [-12, -15, -3, -3, -3, -3, -15, -12],
    [  0,  -3,  0, -1, -1,  0,  -3,   0],
    [ -1,  -3, -1, -1, -1, -1,  -3,  -1],
    [ -1,  -3, -1, -1, -1, -1,  -3,  -1],
    [  0,  -3,  0, -1, -1,  0,  -3,   0],
    [-12, -15, -3, -3, -3, -3, -15, -12],
    [ 30, -12,  0, -1, -1,  0, -12,  30],
];

var BLACK = 1, WHITE = 2;
var data = [];
var myTurn = false;

function init(){
    var b = document.getElementById("board");
    for(var i = 0 ; i < 8 ; i++){
	var tr = document.createElement("tr");
	data[i] = [0,0,0,0,0,0,0,0];
	for(var j = 0 ; j < 8 ; j++){
	    var td = document.createElement("td");
	    td.className = "cell";
	    td.id = "cell" + i + j;
	    td.onclick = clicked;
	    tr.appendChild(td);
	}
	b.appendChild(tr);
    }
    put(3,3,BLACK);
    put(4,4,BLACK);
    put(3,4,WHITE);
    put(4,3,WHITE);
    update();
}

function update(){
    var numWhite = 0,numBlack = 0;
    for(var y = 0 ; y < 8 ; y++){
	for(var x = 0 ; x < 8 ; x++){
	    if(data[y][x] == WHITE){
		numWhite++;
	    }
	    if(data[y][x] == BLACK){
		numBlack++;
	    }
	}
    }
    
    document.getElementById("numBlack").textContent = numBlack;
    document.getElementById("numWhite").textContent = numWhite;

    var blackFlip = canFlip(BLACK);
    var whiteFlip = canFlip(WHITE);

    if(numWhite + numBlack == 64 || (!blackFlip && !whiteFlip)){
	showMessage("GameOver!");
    }
    else if(!blackFlip){
	showMessage("black skip");
	myTurn = false;
    }
    else if(!whiteFlip){
	showMessage("white skip");
	myTurn = true;
    }
    else {
	myTurn = !myTurn;
    }

    if(!myTurn){
	setTimeout(think,1000);
    }
}

function showMessage(str){
    document.getElementById("message").textContent = str;
    setTimeout(function(){
	document.getElementById("message").textContent = "";
    },2000);
}

function clicked(e){
    if(!myTurn){
	return ;
    }
    var id = e.target.id;
    var i = parseInt(id.charAt(4));
    var j = parseInt(id.charAt(5));

    var flipped = getFlipCells(i,j,BLACK);
    if(flipped.length > 0){
	for(var k = 0 ; k < flipped.length ; k++){
	    put(flipped[k][0],flipped[k][1],BLACK);
	}
	put(i,j,BLACK);
	update();
    }
}

function put(i,j,color){
    var c = document.getElementById("cell" + i + j);
    c.textContent = "●";
    c.className = "cell " + (color == BLACK ? "black" : "white");
    data[i][j] = color;
}

function think(){
    var highScore = -1000;
    var px = -1,py = -1;
    for(var y = 0 ; y < 8 ; y++){
	for(var x = 0 ; x < 8 ; x++){
	    var tmpData = copyData();
	    var flipped = getFlipCells(y,x,WHITE);
	    if(flipped.length > 0){
		tmpData[y][x] = WHITE;
		for(var i = 0 ; i < flipped.length ; i++){
		    var p = flipped[i][0];
		    var q = flipped[i][1];
		    tmpData[p][q] = WHITE;		    
		}
		var score = calcWeight(tmpData);
		if(score > highScore){
		    highScore = score;
		    px = x,py = y;
		}
	    }
	}
    }
    
    if(px >= 0 && py >= 0){
	var flipped = getFlipCells(py,px,WHITE);
	if(flipped.length > 0){
	    for(var k = 0 ; k < flipped.length ; k++){
		put(flipped[k][0],flipped[k][1],WHITE);
	    }
	}
	put(py,px,WHITE);
    }
    update();
}

function calcWeight(tmpData){
    var score = 0;
    for(var y = 0 ; y < 8 ; y++){
	for(var x = 0 ; x < 8 ; x++){
	    if(tmpData[y][x] == WHITE){
		score += WeightData[y][x];
	    }
	}
    }
    return score;
}

function copyData(){
    var tmpData = [];
    for(var y = 0 ; y < 8 ; y++){
	tmpData[y] = [];
	for(var x = 0 ; x < 8 ; x++){
	    tmpData[y][x] = data[y][x];
	}
    }
    return tmpData;
}

function canFlip(color){
    for(var y = 0 ; y < 8 ; y++){
	for(var x = 0 ; x < 8 ; x++){
	    var flipped = getFlipCells(y,x,color);
	    if(flipped.length > 0){
		return true;
	    }
	}
    }
    return false;
}

function getFlipCells(i,j,color){
    if(data[i][j] == BLACK || data[i][j] == WHITE){
	return [];
    }

    var dirs = [[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]];
    var result = [];

    for(var d = 0 ; d < dirs.length ; d++){
	var flipped = getFlipCellsOneDir(i,j,dirs[d][1],dirs[d][0],color);
	result = result.concat(flipped);
    }
    return result;
}

function isInBoard(x,y){
    return (0 <= x && x < 8 && 0 <= y && y < 8);
}

function getFlipCellsOneDir(i,j,dy,dx,color){
    var y = i + dy;
    var x = j + dx;
    var flipped = [];

    if(!isInBoard(x,y) || data[y][x] == color || data[y][x] == 0){
	return [];
    }
    flipped.push([y,x]);

    while(true){
	x += dx;
	y += dy;
	if(!isInBoard(x,y) || data[y][x] == 0){
	    return [];
	}
	if(data[y][x] == color){
	    return flipped;
	}
	else {
	    flipped.push([y,x]);
	}
    }
}
